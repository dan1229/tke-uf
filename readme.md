# TKE UF - iOS and Android
### mobile application written using Flutter with Dart

This application was designed for internal use with my fraternity at the University of Florida and is used to coordinate and organize chapter events. It functions as a hub for all information related to the organization - from upcoming events to signup sheets to social media information, any and all important information is at your fingertips.

With Firebase Database integration, Google Calendar integration, and other web services, the application is able to update information in seconds. This enables the announcements/update system, contact updates, dynamic permissions, and many other features.

**Some features the application supports include:**
- Google Calendar integration, allowing members to easily access their shared calendar.
- Update/Announcements system enabling one way communication with the rest of the application owners.
- Dynamic permissions allow admins change what members can perform what actions with just a click.
- Attendance tracking.
- Agenda and event tracking.
- Contact and update system.
- User profiles and sign on.

#### This application is intedend only for internal use, therefore there is no publicly available source or download link.

**Copyright © Daniel Nazarian, all rights reserved.**