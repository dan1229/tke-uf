import 'my_flutter_app_icons.dart';
import 'package:flutter/material.dart';

// Page ========================================================================
class HomePage extends StatelessWidget {

  final List<Widget> _cards = [
    CardUpcomingEvent(),
    CardAnnouncement(),
    CardLinks(),
    CardSocial(),
  ];

  HomePage();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _cards.length,
      itemBuilder: (context, index) {
        return _cards[index];
      }
    );
  }
}



// Cards =======================================================================
class CardUpcomingEvent extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            getTitleRow(Icons.event, 'Upcoming Events'),
            getLine(),
            new ListTile(
              leading: Icon(MyFlutterApp.football),
              trailing: Text("12/29"),
              title: Text(
                "Florida vs. Michigan",
                style: new TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            new ExpansionTile(
                leading: Icon(MyFlutterApp.guidedog),
                title: Text("Service Project"),
                children: <Widget>[
                  new ListTile(
                    leading: Icon(Icons.location_on),
                    title: Text("Animal Shelter"),
                  ),
                  new ListTile(
                    leading: Icon(Icons.alarm),
                    title: Text("9:00 AM - 12:00 PM"),
                  ),
                ]
            ),
            ListTile(
              trailing: Icon(Icons.link, color: Colors.orangeAccent,),
              title: Text(
                "Christmas Shirt Orders Close Today!",
                style: new TextStyle(
                  fontSize: 14,
                  color: Colors.orangeAccent,
                ),
              ),
            ),
          ],
        )
    );
  }
}


class CardAnnouncement extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          getTitleRow(Icons.announcement, 'Anouncements'),
          getLine(),
          new Text(
            "Thursday, 5:24",
            style: new TextStyle(
                color: Colors.black54,
                fontSize: 14,
            ),
          ),
          new Container(
            margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0),
            alignment: Alignment(-1.0, -1.0),
            child: new Text(
              "Andy Buettner",
              textAlign: TextAlign.start,
              style: new TextStyle(
                color: Colors.black87,
                fontSize: 18,
              ),
            ),
          ),
          new Container(
            margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 10.0),
            child: new Text(
              "Hey guys, just want to let you all know we will be holding our philanthropy on Wednesday, February 28th. Excited to see you all there!",
              style: new TextStyle(
                  color: Colors.black54,
                  fontSize: 16
              ),
            ),
          ),
          getLine(),
          new Text(
            "Thursday, 7:51",
            style: new TextStyle(
                color: Colors.black54,
                fontSize: 14,
            ),
          ),
          new Container(
            margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0),
            alignment: Alignment(-1.0, -1.0),
            child: new Text(
              "Josh Piccari",
              textAlign: TextAlign.start,
              style: new TextStyle(
                color: Colors.black87,
                fontSize: 18,
              ),
            ),
          ),
          new Container(
            margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 10.0),
            child: new Text(
              "Reminder that the final round of dues are due in three days. Talk to me if you have questions or concerns.",
              style: new TextStyle(
                  color: Colors.black54,
                  fontSize: 16
              ),
            ),
          ),
        ],
      )
    );
  }
}



class CardLinks extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            getTitleRow(Icons.insert_link, 'Important Links'),
            getLine(),
            new Container(
              margin: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: new Column(
                children: <Widget>[
                  ListTile(
                    trailing: Icon(
                      Icons.star,
                      color: Colors.orange[200],
                    ),
                    title: Text(
                      "Good Standing Points",
                      style: new TextStyle(
                        fontSize: 16,
                        color: Colors.orangeAccent,
                      ),
                    ),
                  ),
                  ListTile(
                    trailing: Icon(
                      Icons.assignment,
                      color: Colors.red,
                    ),
                    title: Text(
                      "Excuse Form",
                      style: new TextStyle(
                        fontSize: 16,
                        color: Colors.orangeAccent,
                      ),
                    ),
                  ),
                  ListTile(
                    trailing: Icon(
                      Icons.update,
                      color: Colors.green,
                    ),
                    title: Text(
                      "Latest Minutes",
                      style: new TextStyle(
                        fontSize: 16,
                        color: Colors.orangeAccent,
                      ),
                    ),
                  ),
                  ListTile(
                    trailing: Icon(
                      Icons.cloud,
                      color: Colors.blue,
                    ),
                    title: Text(
                        "Cloud Folder",
                        style: new TextStyle(
                          fontSize: 16,
                          color: Colors.orangeAccent,
                        ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        )
    );
  }
}



class CardSocial extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            getTitleRow(Icons.account_circle, 'Social Media'),
            getLine(),
            new Container(
              margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Icon(
                    MyFlutterApp.facebook,
                    color: Colors.blue[700],
                    size: 50.0,
                  ),
                  Icon(
                    MyFlutterApp.instagram,
                    color: Colors.purple[700],
                    size: 50.0,
                  ),
                  Icon(
                    MyFlutterApp.twitter,
                    color: Colors.blue[300],
                    size: 50.0,
                  ),
                  Icon(
                    MyFlutterApp.gplus,
                    color: Colors.green[400],
                    size: 50.0,
                  ),
                  Column(
                    children: <Widget>[
                      Icon(
                        Icons.language,
                        color: Colors.red,
                        size: 45.0,
                      ),
                      Text(
                        "uftke.com",
                        style: new TextStyle(
                          color: Colors.red,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        )
    );
  }
}

// Helper Functions ============================================================
Container getTitleRow(IconData icon, String title, {Color color = Colors.black87}) {
  return Container(
    margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Icon(icon, color: color),
        Text(
          '  ' + title,
          style: new TextStyle(
            color: color,
            fontSize: 18,
          ),
        )
      ],
    ),
  );
}

Container getLine({Color color = Colors.black12}) {
  return Container(
    color: color,
    margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0),
    height: 1,
  );
}