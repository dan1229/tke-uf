import 'package:flutter/material.dart';
import 'contacts.dart';
import 'home_page.dart';

// Variables ===================================================================
const int list_offset = 2;


// Contacts Page ===============================================================
class ContactsPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: brothers.length + list_offset,
        itemBuilder: (context, index) {
          return getList(index);
        }
    );
  }
}

// Widgets =====================================================================
class CardExecContact extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children:  getExecTiles(context),
        )
    );
  }
}


// Helper Functions ============================================================
Widget getList(int index) {
  if(index == 0) {
    return CardExecContact();
  }
  else if(index == 1) {
    return Column(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
          alignment: Alignment(-1.0, -1.0),
          child: new Text(
            "Contacts Sheet",
            textAlign: TextAlign.start,
            style: new TextStyle(
              fontSize: 24,
              color: Colors.black87
            ),
          ),
        ),
        getLine(),
      ],
    );
  }
  else {
    return ExpansionTile (
      title: Text(brothers[index - list_offset].name),
      leading: Icon(Icons.account_circle),
      children: <Widget>[
        new Column(
          children: <Widget>[
              getContactInfoTile(brothers[index - list_offset].number, Icons.phone),
              getContactInfoTile(brothers[index - list_offset].email, Icons.email),
          ],
        )
      ],
    );
  }
}

ListTile getContactInfoTile(String s, IconData icon) {
  return ListTile(
    title: Text(s),
    leading: Icon(icon, color: Colors.orangeAccent),
  );
}

List<Widget> getExecTiles(BuildContext context) {
  List<Widget> list = new List();
  list.add(Column(
    children: <Widget>[
      Container(
        margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
        alignment: Alignment(-1.0, -1.0),
        child: new Text(
          "Exec Contacts",
          textAlign: TextAlign.start,
          style: new TextStyle(
              fontSize: 24,
              color: Colors.black87
          ),
        ),
      ),
      getLine(),
    ],
  )
  );

  for(var i = 0; i < 8; ++i)
    list.add(getExecContactTile(context, exec[i], i));

  return list;
}


Widget getExecContactTile(BuildContext context, Brother brother, int i) {
  return InkWell(
    child: new ListTile(
      title: Text(brother.name),
      subtitle: Text(execPositions[i]),
      leading: Icon(execSymbols[i], color: Colors.orangeAccent),
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext c) {
            return AlertDialog(
              title: Text(brother.name),
              content: Text(
                execPositions[i] + '\n\n' +
                'Phone:\n' + brother.number + '\n\n'
                'Email:\n' + execEmails[i] + '\n\n'
                'Personal Email:\n' + brother.email
              ),
            );
          }
        );
      }
    ),
  );
}