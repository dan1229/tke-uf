import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel, WeekDay;

import 'home_page.dart';
import 'my_flutter_app_icons.dart';

class CalendarPage extends StatefulWidget {
  CalendarPage();

  @override
  _CalendarPageState createState() => new _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  DateTime _currentDate = DateTime(2018, 8, 1);
  static Map<DateTime, int> _markedDateMap = {
    DateTime(2018, 11, 20): 2,
    DateTime(2018, 11, 9): 1,
    DateTime(2018, 11, 1): 1,
    DateTime(2018, 11, 3): 1,
    DateTime(2018, 11, 10): 2,
    DateTime(2018, 11, 11): 1,
    DateTime(2018, 10, 29): 1,
    DateTime(2018, 10, 30): 2,
    DateTime(2018, 10, 31): 1,
    DateTime(2018, 11, 5): 1,
    DateTime(2018, 11, 19): 1,
    DateTime(2018, 11, 22): 1,
    DateTime(2018, 11, 25): 1,
    DateTime(2018, 11, 26): 2,
    DateTime(2018, 11, 28): 1,
    DateTime(2018, 11, 16): 3,
    DateTime(2018, 11, 14): 1,
    DateTime(2018, 11, 13): 2,
    DateTime(2018, 11, 30): 2,
    DateTime(2018, 12, 1): 1,
    DateTime(2018, 12, 2): 2,
    DateTime(2018, 12, 3): 1,
    DateTime(2018, 12, 5): 2,
    DateTime(2018, 12, 7): 3,
    DateTime(2018, 12, 8): 1,
    DateTime(2018, 12, 10): 4,
    DateTime(2018, 12, 13): 2,
    DateTime(2018, 12, 14): 2,
    DateTime(2018, 12, 16): 1,
    DateTime(2018, 12, 17): 3,
    DateTime(2018, 12, 18): 2,
    DateTime(2018, 12, 20): 4,
    DateTime(2018, 12, 22): 2,
    DateTime(2018, 12, 23): 1,
    DateTime(2018, 12, 24): 2,
    DateTime(2018, 12, 25): 1,
    DateTime(2018, 12, 29): 1,
  };

  @override
  Widget build(BuildContext context) {
    List<Widget> list = [
      getCalendar(),
      getSelectedDateCard(),
    ];

    return new Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
      new Expanded(
          child: new ListView.builder(
              itemCount: list.length,
              itemBuilder: (BuildContext context, int index) {
                return list[index];
              }))
    ]);
  }

  Widget getCalendar() {
    return Card(
      child: CalendarCarousel(
        height: 400,
        onDayPressed: (DateTime date) {
          this.setState(() => _currentDate = date);
        },
        weekends: [WeekDay.Sunday, WeekDay.Saturday],
        markedDatesMap: _markedDateMap,
        selectedDateTime: _currentDate,
        daysHaveCircularBorder: true,
        headerTextStyle: new TextStyle(color: Colors.black87, fontSize: 24),
        todayBorderColor: Colors.blue[900],
        todayButtonColor: Colors.transparent,
        todayTextStyle: TextStyle(color: Colors.black54),
        selectedDayBorderColor: Colors.orangeAccent,
      ),
    );
  }

  Widget getSelectedDateCard() {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Container(
          margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0),
          child: new Text(
            "December 17, 2018",
            style: new TextStyle(fontSize: 24, color: Colors.black87),
          ),
        ),
        getLine(),
        new ExpansionTile(
          leading: Icon(Icons.account_balance),
          title: Text("Chapter Meeting"),
          children: <Widget>[
            new ListTile(
              leading: Icon(
                Icons.location_on,
                color: Colors.orangeAccent,
              ),
              title: Text("Chapter House"),
            ),
            new ListTile(
              leading: Icon(
                Icons.alarm,
                color: Colors.orangeAccent,
              ),
              title: Text("6:30 - 8:00 PM"),
            ),
          ],
        ),
        new ExpansionTile(
          leading: Icon(MyFlutterApp.basketball),
          title: Text("Intramurals - Basketball"),
          children: <Widget>[
            new ListTile(
              leading: Icon(
                Icons.location_on,
                color: Colors.orangeAccent,
              ),
              title: Text("SW Basketball Courts"),
            ),
            new ListTile(
              leading: Icon(
                Icons.alarm,
                color: Colors.orangeAccent,
              ),
              title: Text("9:30 - 10:00 PM"),
            ),
          ],
        ),
        new ExpansionTile(
          leading: Icon(MyFlutterApp.book_open),
          title: Text("Study Hours"),
          children: <Widget>[
            new ListTile(
              leading: Icon(
                Icons.location_on,
                color: Colors.orangeAccent,
              ),
              title: Text("Marston Science Library, K102"),
            ),
            new ListTile(
              leading: Icon(
                Icons.alarm,
                color: Colors.orangeAccent,
              ),
              title: Text("10:00 PM - 12:00 AM"),
            ),
          ],
        ),
      ],
    );
  }
}
