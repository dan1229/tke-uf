import 'package:flutter/material.dart';
// Class =======================================================================

class Brother {
  String name;
  String email;
  String number;

  Brother(String nam, String email, String num) {
    this.name = nam;
    this.email = email;
    this.number = num;
  }
}

// Const Info ===================================================================
var execPositions = [
  "President",
  "Vice President",
  "Treasurer",
  "New Member Educator",
  "Chaplain",
  "Secretary",
  "Historian",
  "Seargent-At-Arms"
];

var execSymbols = [
  Icons.gavel,
  Icons.description,
  Icons.attach_money,
  Icons.child_care,
  Icons.favorite,
  Icons.brush,
  Icons.photo_camera,
  Icons.fastfood
];

var execEmails = [
  "president@uftke.com",
  "vicepresident@uftke.com",
  "treasurer@uftke.com",
  "newmembereducator@uftke.com",
  "chaplain@uftke.com",
  "secretary@uftke.com",
  "historian@uftke.com",
  "seargentatarms@uftke.com",
];

// Contact Info ================================================================

List<Brother> brothers = [
  new Brother("Alex Wolfe", "alex@tke.com", "555-555-5555"),
  new Brother("Paul Wood", "paul@tke.com", "444-444-4444"),
  new Brother("Tommy Yaegers", "tommy@tke.com", "333-333-3333"),
  new Brother("Greg Fisher", "greg@uftke.com", "555-555-5555"),
  new Brother("Tyler Gallagher", "tyler@uftke.com", "444-444-4444"),
  new Brother("Dominic Brennan", "dom@uftke.com", "444-444-4444"),
  new Brother("Charles Elazar", "cj@uftke.com", "333-333-3333"),
  new Brother("Michael Edmondson", "michael@uftke.com", "999-999-9999"),
  new Brother("Jack Houle", "jackb@uftke.com", "999-999-9999"),
  new Brother("John Zavala", "johng@uftke.com", "999-999-9999"),
  new Brother("Tristan Faubion", "tristan@uftke.com", "999-999-9999"),
  new Brother("Andy Buettner", "greg@uftke.com", "555-555-5555"),
  new Brother("Josh Piccari", "tyler@uftke.com", "444-444-4444"),
  new Brother("Charlie Salamone", "dom@uftke.com", "444-444-4444"),
  new Brother("Leo Fisher", "cj@uftke.com", "333-333-3333"),
  new Brother("David Iusi", "michael@uftke.com", "999-999-9999"),
  new Brother("Daniel Brennan", "jackb@uftke.com", "999-999-9999"),
  new Brother("Angel Franke", "johng@uftke.com", "999-999-9999"),
  new Brother("Jake Wood", "tristan@uftke.com", "999-999-9999"),
];

List<Brother> exec = [
  new Brother("Andy Buettner", "greg@uftke.com", "555-555-5555"),
  new Brother("Josh Piccari", "tyler@uftke.com", "444-444-4444"),
  new Brother("Charlie Salamone", "dom@uftke.com", "444-444-4444"),
  new Brother("Leo Fisher", "cj@uftke.com", "333-333-3333"),
  new Brother("David Iusi", "michael@uftke.com", "999-999-9999"),
  new Brother("Sam Brennan", "jackb@uftke.com", "999-999-9999"),
  new Brother("Angel Franke", "johng@uftke.com", "999-999-9999"),
  new Brother("Jake Wood", "tristan@uftke.com", "999-999-9999"),
];
