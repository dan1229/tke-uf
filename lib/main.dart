import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

import 'contacts_page.dart';
import 'calendar_page.dart';
import 'home_page.dart';


void main(){
  runApp(new MaterialApp(
    home: new MyApp(),
    title: "TKE - Gamma Theta",
    theme: new ThemeData(
      primaryColor: Colors.blue[900],
      accentColor: Colors.orangeAccent,
    )
  ));
}

class MyApp extends StatefulWidget {
  @override
  _SplashScreen createState() => new _SplashScreen();
}

class _SplashScreen extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 3,
        navigateAfterSeconds: new MyHomePage(),
        image: new Image.asset('images/logo.png'),
        photoSize: 150.0,
        title: Text(""),
        backgroundColor: Theme.of(context).primaryColor,
        loaderColor: Theme.of(context).accentColor,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title = 'Tau Kappa Epsilon';

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentIndex = 1;
  final List<Widget> _children = [
    CalendarPage(),
    HomePage(),
    ContactsPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        fixedColor: Theme.of(context).accentColor,
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today),
            title: Text('Calendar'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.contacts),
            title: Text('Contacts')
          )
        ],
      ),
    );
  }

  void onTabTapped(int index)  {
    setState(() {
      _currentIndex = index;
    });
  }
}
